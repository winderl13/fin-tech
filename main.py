import sys
from hw1.hw_1 import main_hw_1
from hw2.hw_2 import main_hw_2
from hw3.hw_3 import main_hw_3
from hw4.hw4 import main_hw4
import store

"hw1/problem1"


def root_main():
    try:
        hw = int(sys.argv[sys.argv.index("-hw") + 1])
    except:
        print("Error! Please specify your hw number by -hw <nr>")
        return
    print(f"Homework {hw}")
    store.init()
    if hw == 1:
        main_hw_1()
        return
    elif hw == 2:
        main_hw_2()
    elif hw == 3:
        main_hw_3()
    elif hw == 4:

        main_hw4()
    else:
        print("Error! Please specify your hw number by -hw <nr>")


if __name__ == '__main__':
    root_main()

import pandas as pd
from tensorflow import keras
import numpy as np

from hw4 import helper


class RNNModel:
    def __init__(self, batch_size: int, input_size: int, rnn_type: str, epochs: int, time_steps: int = 30):
        self.batch_size = batch_size
        self.input_size = input_size
        self.time_steps = time_steps
        self.rnn_type = rnn_type
        self.epochs = epochs
        self.model: keras.Sequential = None
        self.history = None
        self.train_time = None

    def build(self, verbose=1):
        model = keras.Sequential()
        # model.add(keras.layers.Input(shape=(self.time_steps, self.input_size,)))
        if self.rnn_type == "simple":
            model.add(
                keras.layers.SimpleRNN(units=4, activation="relu", input_shape=(self.time_steps, self.input_size),
                                       return_sequences=True))
            model.add(keras.layers.SimpleRNN(units=4, activation="relu", return_sequences=True))
            model.add(keras.layers.SimpleRNN(units=4, activation="relu", return_sequences=False))
        elif self.rnn_type == "lstm":
            model.add(
                keras.layers.LSTM(units=60, activation="relu", input_shape=(self.time_steps, self.input_size),
                                  return_sequences=False))

        elif self.rnn_type == "gru":
            model.add(
                keras.layers.GRU(units=50, activation="relu", input_shape=(self.time_steps, self.input_size),
                                 return_sequences=False))
        else:
            raise ValueError(f"Unknown RNN type: {self.rnn_type}, please implement")
        model.add(keras.layers.Dense(1, activation="relu"))
        self.model = model
        if verbose != 0:
            self.model.summary()
        self.model.build()
        return self

    def compile(self):
        assert self.model is not None
        if self.rnn_type == "simple":
            self.model.compile(loss='mean_squared_error', optimizer=keras.optimizers.RMSprop(learning_rate=0.001))
        elif self.rnn_type == "lstm":
            self.model.compile(loss='mean_squared_error', optimizer=keras.optimizers.RMSprop(learning_rate=0.00005))
        elif self.rnn_type == "gru":
            self.model.compile(loss='mean_squared_error', optimizer=keras.optimizers.RMSprop(learning_rate=0.0001))
        else:
            raise ValueError(f"Unknown RNN type: {self.rnn_type}, please implement")
        return self

    def train(self, train_x, train_y, validation=None, verbose=1):

        train_x, train_y = self._prepare_dataset(train_x, train_y)
        if validation is not None:
            test_x, test_y = self._prepare_dataset(validation[0], validation[1])
            self.history = self.model.fit(train_x, train_y,
                                          batch_size=self.batch_size,
                                          epochs=self.epochs,
                                          validation_data=(test_x, test_y),
                                          verbose=verbose).history
        else:
            self.history = self.model.fit(train_x, train_y,
                                          batch_size=self.batch_size,
                                          epochs=self.epochs,
                                          verbose=verbose).history
        return self

    def _prepare_dataset(self, x_train: pd.DataFrame, y_train: pd.DataFrame):
        x_train = np.asarray(list(helper.chunks(x_train.values, self.time_steps)))
        y_train = np.asarray(y_train[self.time_steps:])
        return x_train, y_train

    def predict_close_value(self, test_x: pd.DataFrame, test_y: pd.DataFrame):
        print(list(test_x.head(0)))
        test_x_p, test_y_p = self._prepare_dataset(test_x, test_y)
        test_y_out = test_y.iloc[30:].copy()
        y = self.model.predict(test_x_p)
        y = list(map(lambda x: x[0], y))
        print(y)
        test_y_out["y_pred"] = y
        return test_y_out

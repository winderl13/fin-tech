import os

import mpl_finance
import store
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import pandas as pd
from sklearn.preprocessing import MinMaxScaler

from hw4 import helper
from hw4.RNNModel import RNNModel


def preprocess_dataset(df):
    df = df.dropna()
    df = df.rename(columns={"Date": "date",
                            "Open": "open",
                            "High": "high",
                            "Low": "low",
                            "Close": "close",
                            "Adj Close": "adj_close",
                            "Volume": "volume",
                            })
    df = helper.compute_sma(df, 30)
    df = helper.compute_sma(df, 30, use_mean=False)
    df = helper.compute_sma(df, 10)
    df = helper.compute_kd(df)
    df = helper.add_additional_features(df)
    df = df.dropna()

    date = df["date"]
    del df["date"]

    df_norm = pd.DataFrame(MinMaxScaler().fit_transform(df.values), columns=list(df.head(0)))

    df_norm["date"] = date
    df["date"] = date

    df_train = df_norm[
        list(map(lambda x: helper.is_datestring_in_range(x, ["1994-01-01", "2016-12-31"]), df["date"]))].copy()
    del df_train["date"]

    df_test = df_norm[
        list(map(lambda x: helper.is_datestring_in_range(x, ["2017-01-01", "2019-12-31"]), df["date"]))].copy()
    del df_test["date"]

    store.dump_dataset(df_train, "gspc_train.csv", "hw4")
    store.dump_dataset(df_test, "gspc_test.csv", "hw4")
    return df_train, df_test, df


def show_plots(df, save=False):
    def my_date(x, pos):
        try:
            return xdate[abs(int(x))]
        except IndexError:
            return ''

    df = df[list(map(lambda x: helper.is_datestring_in_range(x, ["2018-01-01", "2018-12-31"]), df["date"]))]
    df = df.dropna()
    opens = df["open"]
    closes = df["close"]
    highs = df["high"]
    lows = df["low"]

    fig, (ax, ax2, ax3) = plt.subplots(3, 1, figsize=(7.684, 7.684), sharex=True, dpi=600, gridspec_kw={'hspace': 0})
    mpl_finance.candlestick2_ohlc(ax, opens, highs, lows, closes, width=0.5, colorup='g')
    ax.plot(list(range(len(df["MA30"]))), df["MA30"], label="SMA 30")
    ax.plot(list(range(len(df["MA10"]))), df["MA10"], label="SMA 10")
    ax.get_xaxis().set_visible(False)
    ax.set_xticklabels([])
    ax.label_outer()

    ax2.plot(list(range(len(df["K"]))), df["K"])
    ax2.plot(list(range(len(df["D"]))), df["D"])
    ax2.get_xaxis().set_visible(False)
    ax2.set_xticklabels([])
    ax2.label_outer()

    # dates = list(df["date"])[0::n]
    # plt.xticks(ticks=range(len(dates)), labels=list(map(str, dates)))

    bars = ax3.bar(df["date"], df['volume'], 0.5, facecolor='g')
    for i, bar in enumerate(bars):
        if i == 0:
            continue
        h = bar.get_height()
        h0 = bars[i - 1].get_height()

        # Change bar color to red if height less than previous bar
        if h < h0:
            bar.set_facecolor('r')
        elif h == h0:
            bar.set_facecolor('b')
        else:
            bar.set_facecolor('g')

    xdate = [i for i in df["date"]]
    ax.xaxis.set_major_locator(ticker.MaxNLocator(6))
    ax.xaxis.set_major_formatter(ticker.FuncFormatter(my_date))
    if save:
        store.check_create_folder("./store/hw4/plots")
        plt.savefig(f"./store/hw4/plots/candle_stick")
        plt.clf()
        plt.cla()
    else:
        plt.show()


def train_rnn(df_train, df_test, nn_type="simple", save=False):
    x_train = df_train.copy()
    y_train = df_train.loc[:, df_train.columns == 'close'].copy()

    x_test = df_test.copy()
    y_test = df_test.loc[:, df_train.columns == 'close'].copy()
    model = RNNModel(64, x_train.shape[1], nn_type, 20) \
        .build() \
        .compile() \
        .train(x_train, y_train, validation=(x_test, y_test))
    predict_close = model.predict_close_value(x_test, y_test)

    plt.plot(model.history['loss'], label="train loss")
    plt.plot(model.history['val_loss'], label="val loss")
    plt.legend()
    if save:
        store.check_create_folder("./store/hw4/plots")
        plt.savefig(f"./store/hw4/plots/loss_{nn_type}")
        plt.clf()
        plt.cla()
    else:
        plt.show()

    plt.plot(predict_close["close"], label="real")
    plt.plot(predict_close["y_pred"], label="prediction")
    plt.legend()
    if save:
        store.check_create_folder("./store/hw4/plots")
        plt.savefig(f"./store/hw4/plots/close_{nn_type}")
        plt.clf()
        plt.cla()
    else:
        plt.show()


def main_hw4(save=False):
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
    df = store.read_dataset("s_p_500.csv", "hw4")
    df_train, df_test, df = preprocess_dataset(df)
    show_plots(df, save=save)
    train_rnn(df_train, df_test, nn_type="simple", save=save)
    train_rnn(df_train, df_test, nn_type="lstm", save=save)
    train_rnn(df_train, df_test, nn_type="gru", save=save)

import datetime
from talib._ta_lib import BBANDS, MACD, ATR, HT_DCPERIOD


def compute_sma(df, time_period, use_mean=True):
    if use_mean:
        df[f"MA{time_period}"] = df["close"].rolling(window=time_period).mean()
    else:
        df[f"STD{time_period}"] = df["close"].rolling(window=time_period).std()
    return df


def compute_kd(df):
    df['L5'] = df['low'].rolling(window=5).min()
    df['H5'] = df['high'].rolling(window=5).max()
    df['K'] = 100 * ((df['close'] - df['L5']) / (df['H5'] - df['L5']))
    df['D'] = df['K'].rolling(window=3).mean()

    del df['L5']
    del df['H5']
    return df


def is_datestring_in_range(date_str: str, date_range: [str]):
    """
    Helper in order to get the date values in a range
    :param date_range:
    :param date_str:
    :return:
    """
    date_str_parsed = datetime.datetime.strptime(date_str, '%Y-%m-%d')
    date_from = datetime.datetime.strptime(str(date_range[0]), '%Y-%m-%d')
    date_to = datetime.datetime.strptime(str(date_range[1]), '%Y-%m-%d')
    return date_from <= date_str_parsed <= date_to


def chunks(lst, n):
    for i in range(0, len(lst)):
        if i + n < len(lst):
            yield lst[i:i + n]


def add_additional_features(df):
    df["upperband"], df["middleband"], df["lowerband"] = BBANDS(df["close"])
    df["macd"], df["macdsignal"], df["macdhist"] = MACD(df["close"])
    # df["ad"] = AD(df["high"], df["low"], df["close"], df["volume"])
    df["atr"] = ATR(df["high"], df["low"], df["close"], timeperiod=14)
    df["ht_dcperiod"] = HT_DCPERIOD(df["close"])
    return df

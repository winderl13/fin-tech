FROM python:3.7

###################################
# pipenv to make your life easy
###################################
RUN pip install pipenv

ADD . /code

WORKDIR ./code


RUN pipenv install

#CMD tail -f /dev/null
CMD pipenv run python main.py -hw 3

from matplotlib import pyplot as plt

import store
from hw3 import helper
from hw3.CNN_Model import CNNModel
import os
import numpy as np
import gc
import resource
import psutil
from keras import backend as K

from hw3.helper import save_grid_option_on_dict

fm_labels = {
    0: "T-shirt/top",
    1: "Trouser",
    2: "Pullover",
    3: "Dress",
    4: "Coat",
    5: "Sandal",
    6: "Shirt",
    7: "Sneaker",
    8: "Bag",
    9: "Ankle boot"
}


def main_hw_3(perform_gs=False):
    """
    Inital routine of hw3
    :param perform_gs: wether you want to perform a grid search or analyze it
    :return:
    """
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
    ((train_x, train_y), (test_x, test_y)) = helper.load_data()
    train_x, test_x = helper.normalize(train_x, test_x)
    problem_1(train_x, train_y, test_x, test_y, perform_gs)
    model = problem_2(train_x, train_y, test_x, test_y, cached=False, save_file=True)
    problem_3(model, test_x, test_y, save_fig=True)
    problem_4(model, train_x, train_y, save_fig=True)


def get_grid_search_config():
    """
    Get the config with all possible parameters for the grid search
    :return:
    """
    stride_cfgs = [
        [(1, 1), (1, 1), (1, 1)],
        [(1, 2), (2, 1), (1, 1)],
        [(1, 1), (2, 2), (1, 1)],
        [(2, 2), (2, 2), (2, 2)],
    ]
    filter_sizes = [16, 32]
    kernel_sizes = [
        [4, 3, 3],
        [4, 4, 3],
        [4, 4, 4],
        [3, 4, 3],
        [3, 3, 3],
    ]
    denses = [
        [128],
        [256],
        [256, 128],
        [128, 64],

    ]
    return stride_cfgs, filter_sizes, kernel_sizes, denses


def problem_1(train_x, train_y, test_x, test_y, perform_gs):
    """
    Problem 1:
        - perform a grid search
        - use this GS in order to analysze different paramters (stide size, ect.)
    :param train_x:
    :param train_y:
    :param test_x:
    :param test_y:
    :param perform_gs:
    :return:
    """
    nr_searches = grid_search(train_x, train_y, test_x, test_y, cached=not perform_gs)
    if perform_gs:
        return
    analyse_grid_search(nr_searches)


def analyse_grid_search(nr_searches):
    """
    Analyze the GS
    :param nr_searches:
    :return:
    """
    stride_cfgs, filter_sizes, kernel_sizes, denses = get_grid_search_config()
    stride_accs, stride_tts, stride_params = save_grid_option_on_dict(stride_cfgs, nr_searches, 2)
    filter_accs, filter_tts, filter_params = save_grid_option_on_dict(filter_sizes, nr_searches, 0)
    kernel_accs, kernel_tts, kernel_params = save_grid_option_on_dict(kernel_sizes, nr_searches, 1)
    denses_accs, denses_tts, denses_params = save_grid_option_on_dict(denses, nr_searches, 3)

    helper.plot_option(stride_accs, stride_tts, stride_params, "Stride configurations", file_name="stride_cfg")
    helper.plot_option(stride_accs, stride_tts, stride_params, "Stride configurations - Accuracy",
                       file_name="stride_acc",
                       plot_acc_only=True)
    helper.plot_option(filter_accs, filter_tts, filter_params, "Filter configurations", file_name="filter_cfg")
    helper.plot_option(kernel_accs, kernel_tts, kernel_params, "Kernel configurations", file_name="kernel_cfg")
    helper.plot_option(denses_accs, denses_tts, denses_params, "Dense configurations", file_name="dense_cfg")


def grid_search(train_x, train_y, test_x, test_y, cached=False):
    """
    Method that perfomrs the GS
    cached to False if you want to rerun it
    :param train_x:
    :param train_y:
    :param test_x:
    :param test_y:
    :param cached:
    :return:
    """
    stride_cfgs, filter_sizes, kernel_sizes, denses = get_grid_search_config()
    nr_searches = len(stride_cfgs) * len(filter_sizes) * len(kernel_sizes) * len(denses)
    print(f"Performing {nr_searches} searches...")
    if cached:
        print("grid search was performed, returning...")
        return nr_searches
    print("Perfoming grid search for CNN")
    sample_idx = 0
    for stride_cfg in stride_cfgs:
        for filter_size in filter_sizes:
            for kernel_size in kernel_sizes:
                for dense in denses:
                    K.clear_session()
                    print(f"At index nr: {sample_idx}")
                    print(f'RAM Usage: {psutil.virtual_memory().percent}')
                    if str(sample_idx) in [f for f in os.listdir("./store/hw3/models")]:
                        print("Computed, skipping....")
                        sample_idx += 1
                        continue
                    spec = [
                        {"type": "CONV", "filters": filter_size, "k_size": kernel_size[0], "activation": "relu",
                         "strides": stride_cfg[0]},
                        {"type": "MAX_P", "pool_size": (2, 2)},
                        {"type": "CONV", "filters": filter_size, "k_size": kernel_size[1], "activation": "relu",
                         "strides": stride_cfg[1]},
                        {"type": "CONV", "filters": filter_size, "k_size": kernel_size[2], "activation": "relu",
                         "strides": stride_cfg[2]},
                        {"type": "MAX_P", "pool_size": (2, 2)},
                        {"type": "DROP", "drop_val": 0.25},
                        {"type": "FLAT"},
                        {"type": "DENSE", "size": dense[0], "activation": "relu"},
                        {"type": "DENSE", "size": dense[1], "activation": "relu"} if len(dense) > 1 else None,
                        {"type": "DROP", "drop_val": 0.3},
                        {"type": "DENSE", "size": 10, "activation": "softmax"},
                    ]
                    spec = list(filter(lambda x: x is not None, spec))
                    optimizer = {"type": "ADAM", "lr": 0.001}
                    model = CNNModel(spec, optimizer, 500, 25).build(verbose=0) \
                        .compile(verbose=0) \
                        .train(train_x, train_y, validation=(test_x, test_y), verbose=0)
                    model.store(sample_idx, store_model=False)
                    print(f"Acc: {model.history['val_accuracy'][4]}")
                    print(f"Time: {model.training_time:.2f}")
                    print(f"Time: {model.nr_params:.2f}")
                    sample_idx += 1
                    print(f'Max Rss: {resource.getrusage(resource.RUSAGE_SELF).ru_maxrss}')
                    del model.model
                    gc.collect()
    return sample_idx


def problem_2(train_x, train_y, test_x, test_y, cached=False, save_file=False):
    """
    Problem 2:
        - train the optimal specs
        - plot accuracy and loss
    :param train_x:
    :param train_y:
    :param test_x:
    :param test_y:
    :param cached:
    :param save_file:
    :return:
    """
    opt_spec = [
        {"type": "CONV", "filters": 16, "k_size": 3, "activation": "relu", "strides": [1, 1]},
        {"type": "MAX_P", "pool_size": [2, 2]},
        {"type": "CONV", "filters": 16, "k_size": 3, "activation": "relu", "strides": [1, 1]},
        {"type": "CONV", "filters": 16, "k_size": 3, "activation": "relu", "strides": [1, 1]},
        {"type": "MAX_P", "pool_size": [2, 2]},
        {"type": "DROP", "drop_val": 0.25},
        {"type": "FLAT"},
        {"type": "DENSE", "size": 256, "activation": "relu"},
        {"type": "DROP", "drop_val": 0.3},
        {"type": "DENSE", "size": 10, "activation": "softmax"}
    ]
    optimizer = {"type": "ADAM", "lr": 0.001}
    if cached and os.listdir(f"store/hw3/models/best_0"):
        print("Model was cached, using cached weights...")
        model = CNNModel.from_file("best_0", load_m=True)
    else:
        model = CNNModel(opt_spec, optimizer, 500, 25).build() \
            .compile() \
            .train(train_x, train_y, validation=(test_x, test_y))
        model.store("best_0", store_model=True)
    print(f"Time to train the model: {model.training_time}")
    history = model.history
    plt.plot(history['accuracy'], label="train")
    plt.plot(history['val_accuracy'], label="test")
    plt.legend()
    if save_file:
        store.check_create_folder("./store/hw3/plots/")
        plt.savefig("./store/hw3/plots/accuracy")
        plt.cla()
        plt.clf()
    else:
        plt.show()

    plt.plot(history['loss'], label="train")
    plt.plot(history['val_loss'], label="test")
    plt.legend()
    if save_file:
        plt.savefig("./store/hw3/plots/loss")
        plt.cla()
        plt.clf()
    else:
        plt.show()
    return model


def problem_3(model: CNNModel, test_x, test_y, save_fig=False):
    """
    Problem 3:
        - plot the activations of the first layer of the model of P2.
    :param model:
    :param test_x:
    :param save_fig:
    :return:
    """
    plt.imshow(np.squeeze(test_x[0]), cmap='gray')
    plt.title(f"Label = {fm_labels[np.argmax(test_y[0])]}")
    if save_fig:
        store.check_create_folder("./store/hw3/plots/")
        plt.savefig("./store/hw3/plots/image")
        plt.cla()
        plt.clf()
    else:
        plt.show()
    activation = model.get_layer_activation(test_x)
    column_nr = (np.math.sqrt(len(activation[0, 0])))
    if column_nr != int(column_nr):
        row_nr = column_nr = int(column_nr) + 1
    else:
        row_nr = column_nr = int(column_nr)
    fig = plt.figure(figsize=(20, 20))
    for i in range(0, column_nr * row_nr + 1):
        if i >= len(activation[0, 0]):
            break
        fig.add_subplot(row_nr, column_nr, i + 1)
        plt.imshow(activation[:, :, i], cmap='gray')
    if save_fig:
        store.check_create_folder("./store/hw3/plots/")
        plt.savefig("./store/hw3/plots/first_conv")
        plt.cla()
        plt.clf()
    else:
        plt.show()


def problem_4(model: CNNModel, test_x, test_y, columns=4, rows=4, save_fig=False):
    """
    Problem 4:
        - predict 16 samples and show their lables
    :param model:
    :param test_x:
    :param test_y:
    :param columns:
    :param rows:
    :param save_fig:
    :return:
    """
    y_hat = model.predict(test_x)
    figure = plt.figure(figsize=(50, 50))
    for index, i in enumerate(range(1, columns * rows + 1)):
        ax = figure.add_subplot(columns, rows, i, xticks=[], yticks=[])
        ax.imshow(np.squeeze(test_x[index]), cmap='gray')
        p_index = int(np.argmax(y_hat[index]))
        t_index = int(np.argmax(test_y[index]))
        ax.set_title(f"{fm_labels[p_index]} ({fm_labels[t_index]})",
                     color=("green" if p_index == t_index else "red"),
                     fontsize=50)
    if save_fig:
        store.check_create_folder("./store/hw3/plots/")
        plt.savefig("./store/hw3/plots/predictions")
        plt.cla()
        plt.clf()
    else:
        plt.show()

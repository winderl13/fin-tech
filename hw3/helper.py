from statistics import median, mean

from keras.datasets import fashion_mnist
from keras.utils import to_categorical
from matplotlib import pyplot as plt

import store
from hw3.CNN_Model import CNNModel


def plot_option(accuracy, tts, params, title, file_name=None, plot_acc_only=False):
    plt.scatter(list(accuracy.keys()), accuracy.values(), label="accuracy")
    if not plot_acc_only:
        plt.scatter(list(tts.keys()), tts.values(), label="training time")
        plt.scatter(list(params.keys()), params.values(), label="nr params")
    plt.title(title)
    plt.legend()
    if file_name is not None:
        store.check_create_folder("./store/hw3/plots")
        plt.savefig(f"./store/hw3/plots/{file_name}")
        plt.clf()
        plt.cla()
    else:
        plt.show()


def load_data():
    """
    Loading the MINIST Fashion Dataset
    Reshaping the training and testinng data in order to get one channel
    transforming the Y to a categorical data
    :return: a tuple of training a tuple of testing data: (train_x, train_y), (test_x, test_y)
    """

    ((train_x, train_y), (test_x, test_y)) = fashion_mnist.load_data()
    train_x = train_x.reshape((train_x.shape[0], 28, 28, 1))
    test_x = test_x.reshape((test_x.shape[0], 28, 28, 1))
    train_y = to_categorical(train_y)
    test_y = to_categorical(test_y)
    return (train_x, train_y), (test_x, test_y)


def normalize(train, test):
    """
    Normalize the datasets pixel values
    Since they are in [0;255] => divide by 255.0 to reshape to [0;1]
    :param train:
    :param test:
    :return: train, test (normalized)
    """
    train_norm = train.astype('float32')
    test_norm = test.astype('float32')
    train_norm = train_norm / 255.0
    test_norm = test_norm / 255.0
    return train_norm, test_norm


def grid_search_params_from_layer_specs(layer_specs):
    filters = layer_specs[0]["filters"]
    kernels = [
        (layer_specs[0]["k_size"]),
        (layer_specs[2]["k_size"]),
        (layer_specs[3]["k_size"])
    ]
    strides = [
        tuple(layer_specs[0]["strides"]),
        tuple(layer_specs[2]["strides"]),
        tuple(layer_specs[3]["strides"]),
    ]

    if len(layer_specs) == 10:
        denses = [
            layer_specs[7]["size"],
        ]
    else:
        assert len(layer_specs) == 11
        denses = [
            layer_specs[7]["size"],
            layer_specs[8]["size"]
        ]
    return filters, kernels, strides, denses


def save_grid_option_on_dict(grid_option, nr_searches, option_idx):
    accs = {}
    tts = {}
    params = {}
    for option in grid_option:
        acc_values = []
        tt_values = []
        param_values = []
        all_tts = []
        all_params = []
        for sample_idx in range(nr_searches):
            model = CNNModel.from_file(sample_idx, load_m=False)
            out = grid_search_params_from_layer_specs(model.layer_specs)
            all_tts.append(model.training_time)
            all_params.append(model.nr_params)
            if out[option_idx] == option:
                acc_values.append(model.history["val_accuracy"][-1])
                tt_values.append(model.training_time)
                param_values.append(model.nr_params)
        accs[str(option)] = median(acc_values)
        tt_values = list(map(lambda x: x / max(all_tts), tt_values))
        param_values = list(map(lambda x: x / max(all_params), param_values))
        tts[str(option)] = mean(list(tt_values))
        params[str(option)] = mean(list(param_values))
    return accs, tts, params

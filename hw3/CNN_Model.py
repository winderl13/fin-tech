from keras import Sequential
from keras.layers import *
from keras.optimizers import Adam
import store
from keras.models import load_model
from keras import models
import numpy as np
import time


class CNNModel():
    def __init__(self, layer_specs, optimizer, b_size, epochs):
        self.layer_specs = layer_specs
        self.optimizer = optimizer
        self.b_size = b_size
        self.epochs = epochs
        self.model: Sequential = None
        self.history = None
        self.training_time = None
        self.nr_params = None

    def build(self, verbose=1):
        model = Sequential()
        for index, layer_spec in enumerate(self.layer_specs):
            if layer_spec["type"] == "CONV":
                if index == 0:
                    model.add(
                        Conv2D(layer_spec["filters"],
                               kernel_size=layer_spec["k_size"],
                               activation=layer_spec["activation"],
                               strides=layer_spec["strides"] if layer_spec["strides"] is not None else (1, 1),
                               padding="same",
                               kernel_initializer='he_uniform',
                               input_shape=(28, 28, 1)))
                else:
                    model.add(
                        Conv2D(layer_spec["filters"],
                               kernel_size=layer_spec["k_size"],
                               activation=layer_spec["activation"],
                               padding="same",
                               kernel_initializer='he_uniform',
                               strides=layer_spec["strides"] if layer_spec["strides"] is not None else (1, 1)))
            elif layer_spec["type"] == "MAX_P":
                if layer_spec["pool_size"] is not None:
                    model.add(MaxPooling2D(layer_spec["pool_size"]))
                else:
                    model.add(MaxPooling2D())
            elif layer_spec["type"] == "FLAT":
                model.add(Flatten())
            elif layer_spec["type"] == "DENSE":
                model.add(
                    Dense(layer_spec["size"], activation=layer_spec["activation"])
                )
            elif layer_spec["type"] == "DROP":
                model.add(Dropout(layer_spec["drop_val"]))
            elif layer_spec["type"] == "B_NORM":
                model.add(BatchNormalization(axis=1))
            else:
                raise Exception(f"Unknown specs: {layer_spec}")
        self.model = model
        if verbose != 0:
            self.model.summary()
        self.model.build()
        return self

    def compile(self, verbose=1):
        assert self.model is not None
        if self.optimizer["type"] == "ADAM":
            optimizer = Adam(learning_rate=self.optimizer["lr"])
        else:
            raise Exception(f"Unknown optimizer: {self.optimizer}")
        self.model.compile(optimizer, loss='categorical_crossentropy', metrics=['accuracy'])
        return self

    def train(self, train_x, train_y, validation=None, verbose=1):
        assert self.model is not None
        if validation is None:
            now = time.time()
            self.history = self.model.fit(train_x, train_y, batch_size=self.b_size, epochs=self.epochs,
                                          verbose=verbose).history
            self.training_time = time.time() - now
            self.nr_params = self.model.count_params()
        else:
            assert validation is not None
            now = time.time()
            self.history = self.model.fit(x=train_x, y=train_y,
                                          epochs=self.epochs,
                                          batch_size=self.b_size,
                                          validation_data=validation,
                                          verbose=verbose
                                          ).history
            self.training_time = time.time() - now
            self.nr_params = self.model.count_params()
        return self

    def evaluate(self, test_x, test_y):
        assert self.model is not None and self.history is not None
        return self.model.evaluate(test_x, test_y)

    def store(self, m_id, store_model=True):
        save_dir = f"./store/hw3/models/{m_id}"
        store.check_create_folder(save_dir)
        model_cfg = {
            "layer_spec": self.layer_specs,
            "b_size": self.b_size,
            "epochs": self.epochs,
            "optimizer": self.optimizer,
            "trained": bool(self.history is not None)
        }
        if self.history is not None:
            n_hist = {}
            for k, v in dict(self.history).items():
                n_hist[k] = list(map(float, v))
            model_cfg["history"] = dict(n_hist)
            model_cfg["training_time"] = self.training_time
            model_cfg["nr_params"] = self.nr_params

            if store_model:
                self.model.save(f"{save_dir}/model.h5")
        store.save_json(model_cfg, f"m_cfg", save_dir)

    def get_layer_activation(self, img_tensor, n=0, sample_index=0):
        layer_outputs = [layer.output for layer in self.model.layers]
        activation_model = models.Model(inputs=self.model.input, outputs=layer_outputs)
        activations = activation_model.predict(img_tensor)
        if n >= len(activations):
            raise ValueError("Activation index out of bounds")
        first_layer_activation = activations[n]
        return first_layer_activation[sample_index]

    def predict(self, x):
        return self.model.predict(x)

    @staticmethod
    def from_file(m_id, load_m=True):
        save_dir = f"./store/hw3/models/{m_id}"
        cfg = store.load_json(f"m_cfg", save_dir)
        cnn_m = CNNModel(cfg["layer_spec"], cfg["optimizer"], cfg["b_size"], cfg["epochs"])
        if cfg["trained"]:
            cnn_m.history = cfg["history"]
            cnn_m.training_time = cfg["training_time"]
            cnn_m.nr_params = cfg["nr_params"]
        if load_m:
            model = load_model(f"{save_dir}/model.h5")
            cnn_m.model = model
        return cnn_m

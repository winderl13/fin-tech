import pandas as pd
from functools import reduce
import numpy as np
import os
import json

base_dir = "./store"
initialized = False


def init(new_base_dir="./store"):
    global base_dir
    base_dir = new_base_dir
    global initialized
    initialized = True


def check_create_folder(folder):
    if not os.path.exists(folder):
        os.makedirs(folder)


def read_dataset(file_name, file_dir="", sep=","):
    assert initialized
    check_create_folder(f"{base_dir}/{file_dir}")

    if file_dir == '':
        return pd.read_csv(f"{base_dir}/{file_name}", sep=sep)
    else:
        return pd.read_csv(f"{base_dir}/{file_dir}/{file_name}", sep=sep)


def dump_dataset(df, file_name, file_dir="", sep=","):
    assert initialized
    if not os.path.exists(f"{base_dir}/{file_dir}"):
        os.makedirs(f"{base_dir}/{file_dir}")

    if file_dir == '':
        return df.to_csv(f"{base_dir}/{file_name}", index=False, sep=sep)
    else:
        return df.to_csv(f"{base_dir}/{file_dir}/{file_name}", index=False, sep=sep)


def split_dataset(df, cat_distribution, shuffel=True):
    assert initialized
    if shuffel:
        df = df.sample(frac=1)
    all_size = 0
    split_positions = []
    for cat_dist in cat_distribution:
        n_size = int(len(df) * cat_dist)
        split_positions.append(all_size + n_size)
        all_size += n_size
    data_frames = np.array_split(df, split_positions)
    return data_frames


def read_and_split_dataset(cat_distribution, file_name, file_dir=""):
    assert initialized
    assert reduce(lambda x, y: x + y, cat_distribution) == 1.0
    if file_dir == '':
        df = pd.read_csv(f"{base_dir}/{file_name}")
    else:
        df = pd.read_csv(f"{base_dir}/{file_dir}/{file_name}")

    print(df.head(1))
    return split_dataset(df, cat_distribution)


def remove_columns(df, left_headers):
    for header in df.head(0):
        if header not in left_headers:
            del df[header]
            continue


def prepare_one_hot_column(df, boolean_correspondence=None):
    if boolean_correspondence is None:
        boolean_correspondence = ["yes", "no"]

    data_types = get_data_types(df)
    for index, header in enumerate(df.head(0)):
        if np.issubdtype(df[header], np.number):
            # default case colunm is of type number therfore no one hot is necessary
            continue
        else:
            if len(data_types[header]) == 2 and not all(x in data_types[header] for x in boolean_correspondence):
                # header is in a A or B category but not specific boolean
                df[header] = list(map(lambda x: int(x == data_types[header][0]), df[header]))
                df.rename({header: f"{header}_{data_types[header][0]}"}, axis=1, inplace=True)
            elif all(x in data_types[header] for x in boolean_correspondence):
                # header is boolean according to boolean_correspondence
                df[header] = list(map(lambda x: int(x == data_types[header][0]), df[header]))
            else:
                # case three many categories, map all values into the header and add either 1/0 if the cat is set or not
                for category in data_types[header]:
                    cat_array = list(map(lambda x: int(x == category), df[header]))
                    df.insert(loc=index, column=f"{header}_is_{category}", value=cat_array)
                del df[header]


def get_data_types(df):
    assert initialized
    heading_types = {}
    for heading in df.head(0):
        heading_types[heading] = list(set(df[heading]))
        # print(f"{heading}".ljust(15) + "=>" + f"\t{set(df[heading])}")
    return heading_types


def save_json(data, file_name, path):
    with open(f'{path}/{file_name}.json', 'w') as f:
        json.dump(data, f)  # TODO fix it for float32 for acc data


def load_json(file_name, path):
    with open(f'{path}/{file_name}.json', 'r') as f:
        return json.load(f)

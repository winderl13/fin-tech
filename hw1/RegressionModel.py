import numpy as np
import pandas as pd
import time

import store


class RegressionModel:
    def __init__(self, mode="std", p_lambda=1.0, alpha=1.0):
        self.w = None
        self.w_0 = None
        self.alpha = alpha
        self.mode = mode
        self.p_lambda = p_lambda

    def fit(self, x, y):
        """
        fit/training function
        :param x:
        :param y:
        :return:
        """
        dot = np.dot
        if self.mode == "std":
            pseudo_inverse = np.dot(np.linalg.inv(np.dot(x.T, x)), x.T)
            self.w = np.dot(pseudo_inverse, y)
        elif self.mode == "reg":
            m = x.shape[0]
            inverse = np.linalg.inv(1 / m * dot(x.T, x) + self.p_lambda * np.identity(x.shape[1]))
            self.w = dot(inverse, 1 / m * dot(x.T, y))
        elif self.mode == "reg_bias":
            x['offset'] = 1  # to implement the biased term
            inverse = np.linalg.inv(dot(x.T, x) + self.p_lambda * np.identity(x.shape[1]))
            self.w = np.dot(inverse, dot(x.T, y))
        elif self.mode == "bay":
            x['offset'] = 1  # to implement the biased term
            inverse = np.linalg.inv(np.dot(x.T, x) + self.alpha * np.identity(x.shape[1]))
            self.w = np.dot(inverse, np.dot(x.T, y))
            pass
        else:
            raise Exception(f"mode: {self.mode} is not implemented")
        assert self.w is not None
        return self

    def predict(self, x):
        """
        prediction
        :param x:
        :return:
        """
        assert self.w is not None
        if self.mode == "std":
            return np.dot(x, self.w)
        elif self.mode == "reg":
            return np.dot(x, self.w)
        elif self.mode == "reg_bias":
            x = np.append(np.array(x), 1)
            return np.dot(x, self.w)
        elif self.mode == "bay":
            x = np.append(np.array(x), 1)
            return np.dot(x, self.w)
        else:
            raise Exception(f"mode: {self.mode} is not implemented")

    def predict_and_compute_mse(self, x_test, y_test):
        y_hat = [self.predict(x[1]) for x in x_test.iterrows()]
        return np.sqrt(np.mean((y_hat - np.array(y_test)) ** 2))

    def store_weights(self):
        assert self.w is not None
        timestamp = int(round(time.time() * 1000))
        print(f"weights at: hw1/weights/BasicRegressionModel_mode_{self.mode}_weights_{timestamp}")
        store.dump_dataset(pd.DataFrame(self.w),
                           f"BasicRegressionModel_mode_{self.mode}_weights_{int(round(time.time() * 1000))}",
                           "hw1/weights")
        return timestamp

    def from_weights(self, timestamp):
        w_df = store.read_dataset(f"BasicRegressionModel_mode_{self.mode}_weights_{timestamp}",
                                  "hw1/weights")
        self.w = w_df.values
        return self

import store
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from hw1.RegressionModel import RegressionModel


def normalize_dataset(df, means, stds):
    for header in df.head(0):
        if header == "ID" or header == "G3":
            continue
        mean = means[header]
        std = stds[header]
        df[header] = list(map(lambda x: (x - mean) / std, df[header]))


headers = [
    'ID',
    'school',
    'sex',
    'age',
    'famsize',
    'studytime',
    'failures',
    'activities',
    'higher',
    'internet',
    'romantic',
    'famrel',
    'freetime',
    'goout',
    'Dalc',
    'Walc',
    'health',
    'absences',
    'G3'
]


def main_hw_1():
    """
    Mainfunction for the hw1
    there where two tasks Task 1 and Task 2
    :return:
    """
    print("--- Task 1 ---")
    timestamp = task_1()
    print("--- Task 2 ---")
    task_2(timestamp)


def task_1():
    """
    1. Linear Regression
    """
    store.init("./store")
    obtain_training_set()
    rmse_b = do_regression_b()
    rmse_c = do_regression_c()
    rmse_d, timestamp = do_regression_d()
    rmse_e = do_regression_e()
    rmses = {
        "b": rmse_b,
        "c": rmse_c,
        "d": rmse_d,
        "e": rmse_e
    }
    plot_results(rmses)
    return timestamp


def obtain_training_set():
    """
    Subtask a)
    Get the training sets and store them to hw1/split
    """

    df = store.read_dataset("train.csv", file_dir="hw1")
    store.remove_columns(df, headers)
    store.prepare_one_hot_column(df)

    data_sets = store.split_dataset(df, [0.8, 0.2], shuffel=False)
    training_means = {header: np.mean(data_sets[0][header]) for header in df.head(0)}
    training_std = {header: np.std(data_sets[0][header]) for header in df.head(0)}
    normalize_dataset(data_sets[0], training_means, training_std)
    normalize_dataset(data_sets[1], training_means, training_std)
    store.dump_dataset(data_sets[0], f"train_out_{len(data_sets[0])}.csv", file_dir="hw1/split")
    store.dump_dataset(data_sets[1], f"train_out_{len(data_sets[1])}.csv", file_dir="hw1/split")


def do_regression_b():
    """
    b)

    Basic learning algorithm with LR

    - Method to get RMSE from test set (200 samples)
        => it was computed with the stored weights to 0.06482647913159847
    """
    print("B)")
    df = store.read_dataset(f"train_out_800.csv", file_dir="hw1/split")
    del df['ID']
    y = df.loc[:, df.columns == 'G3'].copy()
    x = df.loc[:, df.columns != 'G3'].copy()
    model = RegressionModel().fit(x, y)
    model.store_weights()

    df = store.read_dataset(f"train_out_200.csv", file_dir="hw1/split")
    del df['ID']
    y = df.loc[:, df.columns == 'G3'].copy()
    x = df.loc[:, df.columns != 'G3'].copy()

    MSE = model.predict_and_compute_mse(x, y)
    print("RMSE: ", model.predict_and_compute_mse(x, y))

    predicted_y = [model.predict(x_vector) for index, x_vector in x.iloc[:].iterrows()]
    store.dump_dataset(pd.DataFrame(predicted_y), "prediction_b.csv", "hw1/prediction")
    print()
    return MSE


def do_regression_c():
    """
    Perform a Linear Regression with the regularized model to avoid overfitting

    - Method to get RMSE from test set (200 samples)
        => it was computed with the stored weights to 0.06293615672719635

    - Obtained weights are stored with store/weights/BasicRegressionModel_mode_reg_weights_{currentTimeMs}
    """
    print("C)")
    df = store.read_dataset(f"train_out_800.csv", file_dir="hw1/split")
    del df['ID']
    y = df.loc[:, df.columns == 'G3'].copy()
    x = df.loc[:, df.columns != 'G3'].copy()
    model = RegressionModel(mode="reg").fit(x, y)
    model.store_weights()

    df1 = store.read_dataset(f"train_out_200.csv", file_dir="hw1/split")
    del df1['ID']
    y = df1.loc[:, df1.columns == 'G3'].copy()
    x = df1.loc[:, df1.columns != 'G3'].copy()
    rmse = model.predict_and_compute_mse(x, y)
    print("RMSE: ", model.predict_and_compute_mse(x, y))

    predicted_y = [model.predict(x_vector) for index, x_vector in x.iloc[:].iterrows()]
    store.dump_dataset(pd.DataFrame(predicted_y), "prediction_c.csv", "hw1/prediction")
    print()
    return rmse


def do_regression_d():
    """
    Perform a Linear Regression with the regularized model to avoid overfitting

    - Method to get RMSE from test set (200 samples)
        => it was computed with the stored weights to 0.06293615672719635

    - Obtained weights are stored with store/weights/BasicRegressionModel_mode_reg_weights_{currentTimeMs}
    """
    print("D)")
    df = store.read_dataset(f"train_out_800.csv", file_dir="hw1/split")
    del df['ID']
    y = df.loc[:, df.columns == 'G3'].copy()
    x = df.loc[:, df.columns != 'G3'].copy()
    model = RegressionModel(mode="reg_bias", p_lambda=1.0).fit(x, y)
    timestamp = model.store_weights()

    df1 = store.read_dataset(f"train_out_200.csv", file_dir="hw1/split")
    del df1['ID']
    y = df1.loc[:, df1.columns == 'G3'].copy()
    x = df1.loc[:, df1.columns != 'G3'].copy()
    rmse = model.predict_and_compute_mse(x, y)
    print("RMSE: ", model.predict_and_compute_mse(x, y))

    predicted_y = [model.predict(x_vector) for index, x_vector in x.iloc[:].iterrows()]
    store.dump_dataset(pd.DataFrame(predicted_y), "prediction_d.csv", "hw1/prediction")
    print()
    return rmse, timestamp


def do_regression_e():
    """
    Perform a Linear Regression with the regularized model to avoid overfitting

    - Method to get RMSE from test set (200 samples)
        => it was computed with the stored weights to 0.06293615672719635

    - Obtained weights are stored with store/weights/BasicRegressionModel_mode_reg_weights_{currentTimeMs}
    """
    print("E)")
    df = store.read_dataset(f"train_out_800.csv", file_dir="hw1/split")
    del df['ID']
    y = df.loc[:, df.columns == 'G3'].copy()
    x = df.loc[:, df.columns != 'G3'].copy()
    model = RegressionModel(mode="bay", alpha=1.0).fit(x, y)
    model.store_weights()

    df1 = store.read_dataset(f"train_out_200.csv", file_dir="hw1/split")
    del df1['ID']
    y = df1.loc[:, df1.columns == 'G3'].copy()
    x = df1.loc[:, df1.columns != 'G3'].copy()
    rmse = model.predict_and_compute_mse(x, y)
    print("RMSE: ", model.predict_and_compute_mse(x, y))

    predicted_y = [model.predict(x_vector) for index, x_vector in x.iloc[:].iterrows()]
    store.dump_dataset(pd.DataFrame(predicted_y), "prediction_e.csv", "hw1/prediction")
    print()
    return rmse


def plot_results(rmse):
    """
    Plot the different predictions into a diagram against the ground truth
    :param rmse:
    :return:
    """
    print("F)")
    gt = store.read_dataset(f"train_out_200.csv", file_dir="hw1/split")

    gt = gt.loc[:, gt.columns == 'G3'].copy()
    df_b = store.read_dataset("prediction_b.csv", "hw1/prediction")
    df_c = store.read_dataset("prediction_c.csv", "hw1/prediction")
    df_d = store.read_dataset("prediction_d.csv", "hw1/prediction")
    df_e = store.read_dataset("prediction_e.csv", "hw1/prediction")

    plt.plot(gt, label=f"Ground Truth")
    plt.plot(df_b, label=f"({round(rmse['b'], 2)}) Linear Regression")
    plt.plot(df_c, label=f"({round(rmse['c'], 2)}) Linear Regression (reg)")
    plt.plot(df_d, label=f"({round(rmse['d'], 2)}) Linear Regression (b/r)")
    plt.plot(df_e, label=f"({round(rmse['e'], 2)}) Bayesian Linear Regression")
    axes = plt.gca()
    axes.set_ylim([-20, 20])
    plt.xlabel("sample index")
    plt.ylabel("values")
    plt.legend()
    plt.savefig("./store/hw1/plot.png")
    print(f"plot at: hw1/plot.png")
    print()


def task_2(timestamp):
    """
    2. Hidden Test Set
    :param timestamp: timestamp of the training weights from 1.
    """
    x = store.read_dataset("test_no_G3.csv", file_dir="hw1")
    ids = (x.iloc[:, 0].copy())
    store.remove_columns(x, headers)
    store.prepare_one_hot_column(x)
    del x['ID']
    means = {header: np.mean(x[header]) for header in x.head(0)}
    std = {header: np.std(x[header]) for header in x.head(0)}
    normalize_dataset(x, means, std)

    model = RegressionModel(mode="reg_bias", p_lambda=1.0).from_weights(timestamp)
    y_hat = [model.predict(x_vector)[0] for index, x_vector in x.iloc[:].iterrows()]
    out = pd.DataFrame({
        "ID": ids,
        "prediction": y_hat
    })
    store.dump_dataset(out, "T08902115_1.txt", "hw1", sep="\t")
    print(f"prediction at: hw1/T08902115_1.txt")

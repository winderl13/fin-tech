import tensorflow as tf
import matplotlib.pyplot as plt
import seaborn as sn
from sklearn.preprocessing import StandardScaler
import store


def normalize_data(df, headers):
    std_sc = StandardScaler()
    for header in headers:
        df[header] = std_sc.fit_transform([df[header].values])[0]


def prepare_dataset(df, batch_size=None, use_y=True):
    x_train, y_train = split_dataset(df, do_one_hot_encoding=use_y)
    ds = tf.data.Dataset.from_tensor_slices((x_train.values, y_train.values))
    if batch_size is not None:
        ds = ds.batch(batch_size)
    if use_y:
        return ds, x_train.shape[1]
    else:
        return ds


def split_dataset(df, do_one_hot_encoding=False):
    y_train = df.loc[:, df.columns == 'Class'].copy()
    if do_one_hot_encoding:
        y_train['Class_0'] = list(map(lambda x: int(x == 0), y_train['Class']))
        y_train['Class_1'] = list(map(lambda x: int(x == 1), y_train['Class']))
        del y_train['Class']
    x_train = df.loc[:, df.columns != 'Class'].copy()
    normalize_data(x_train, ["Time", "Amount"])
    return x_train, y_train


def get_prediction_df(df, batch_size):
    d_x, d_y = split_dataset(df)
    d_x = tf.data.Dataset.from_tensor_slices((d_x.values, d_y.values)).batch(batch_size)
    d_y = tf.compat.v2.convert_to_tensor(d_y.values.flatten())
    return d_x, d_y


def compute_params(cm):
    # Class 0
    tp_c0 = cm[0][0]
    fp_c0 = cm[0][1]
    fn_c0 = cm[1][0]
    precision_c0 = tp_c0 / float((tp_c0 + fp_c0))
    recall_c0 = tp_c0 / float((tp_c0 + fn_c0))
    f_1_c0 = 2 * precision_c0 * recall_c0 / (precision_c0 + recall_c0)
    params_c0 = (precision_c0, recall_c0, f_1_c0)
    # Class 1
    tp_c1 = cm[1][0]
    fp_c1 = cm[1][1]
    fn_c1 = cm[0][0]
    precision_c1 = tp_c1 / float((tp_c1 + fp_c1))
    recall_c1 = tp_c1 / float((tp_c1 + fn_c1))
    f_1_c1 = 2 * precision_c1 * recall_c1 / (precision_c1 + recall_c1)
    params_c1 = (precision_c1, recall_c1, f_1_c1)
    return params_c0, params_c1


def save_show_plot(save_name):
    if save_name is None:
        plt.show()
        plt.clf()
        plt.cla()
        plt.close()
    else:
        store.check_create_folder(f"{store.base_dir}/hw2/pics/")
        plt.savefig(f"{store.base_dir}/hw2/pics/{save_name}.png")
        plt.clf()
        plt.cla()
        plt.close()


def plot_confusion_matrix(cm_data, title=None, save_name=None):
    fig, ax = plt.subplots(figsize=(3, 3))
    sn.heatmap(cm_data, ax=ax, xticklabels=1, cmap="YlGnBu",
               annot=True, fmt="d", annot_kws={"size": 15})  # font size
    plt.yticks([0, 2], ('True-0', 'True-1'), rotation=1, fontsize="10")
    plt.xticks([0, 2], ('Pred-0', 'Pred-1'), rotation=1, fontsize="10")
    plt.title(title)
    save_show_plot(save_name)


def plot_loss_acc(history, save_name=None):
    plt.figure(figsize=(9, 4))
    plt.subplot(121)
    plt.plot(history['accuracy'], label="train")
    plt.plot(history['val_accuracy'], label="test")
    plt.legend()
    plt.xlabel("epochs")
    plt.ylabel("accuracy")
    plt.subplot(122)
    plt.plot(history['loss'], label="train")
    plt.plot(history['val_loss'], label="test")
    plt.legend()
    plt.xlabel("epochs")
    plt.ylabel("loss")
    save_show_plot(save_name)


def plot_true_false_positives(fp, tp, mean_auc, save_name=None):
    plt.plot(100 * fp, 100 * tp, linewidth=2, label=f"AUC {mean_auc:.3f}")
    plt.plot([i for i in range(100)], [i for i in range(100)], linewidth=1, color="red", linestyle='dashed')
    plt.xlabel('False positives [%]')
    plt.ylabel('True positives [%]')
    plt.title('Receiver operating characteristic')
    plt.grid(True)
    plt.legend()
    ax = plt.gca()
    ax.set_aspect('equal')
    save_show_plot(save_name)


def plot_precision_recall(recall, precision, avg_precission, save_name=None):
    plt.figure()
    plt.step(100 * recall, 100 * precision, where='post')
    plt.fill_between(100 * recall, 100 * precision, alpha=0.2, color='b')
    plt.xlabel('Recall [%]')
    plt.ylabel('Precision [%]')
    plt.xlim((0, 100))
    plt.ylim((0, 100))
    plt.title(f"Precision-Recall curve: AP={avg_precission:.2f}")
    save_show_plot(save_name)

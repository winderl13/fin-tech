import store
import os
import ast
import itertools
from hw2.DNN_Model import DNNModel
from operator import itemgetter
from tensorboard.plugins.hparams import api as hp
import tensorflow as tf
from hw2 import helper
from sklearn import metrics
from sklearn.metrics import precision_recall_curve, confusion_matrix, accuracy_score, auc, average_precision_score
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
import pandas as pd
from warnings import simplefilter

LAYER_SPEC: hp.HParam = None
LEARN_RATE: hp.HParam = None
NR_ITERATIONS: hp.HParam = None
HP_BATCH_SIZE: hp.HParam = None


def main_hw_2():
    """
    Mainfunction for the hw2
    :return:
    """
    store.init("./store")
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

    print("--- Task 1 ---")
    task_1(perform_grid=False)

    print("--- Task 2 ---")
    task_2()


def task_1(perform_preprocess=True, perform_grid=False):
    if perform_preprocess:
        data_preprocess()
    if perform_grid:
        subtask_i_a()
    model = subtask_i_b()
    train_cm, test_cm = subtask_ii(model)
    subtask_iii(train_cm, test_cm)
    # subtask iV in report
    subtask_v()
    subtask_vi(model)


def task_2():
    print()
    df_train = store.read_dataset(f"train_out_1120.csv", file_dir="hw2/split")
    df_test = store.read_dataset(f"train_out_280.csv", file_dir="hw2/split")
    print("Refitting model...")
    dnn_model = DNNModel(
        [25, 10],  # layer spec
        0.001,  # learning rate
        50,  # epochs
        32  # batch size
    ) \
        .set_train_test_data(df_train, df_test) \
        .fit(verbose=0)
    print("Done! Prediction")
    df_test_no_class = store.read_dataset("test_no_class.csv", file_dir="hw2/")
    df_test_no_class = helper.prepare_dataset(df_test_no_class, batch_size=dnn_model.batch_size, use_y=False)
    prediction = dnn_model.predict(df_test_no_class)
    out = pd.DataFrame({
        "Class": prediction,
    })
    store.dump_dataset(out, "T08902115_answer.txt", "hw2", sep="\t")


def data_preprocess():
    """
    Basic function in the beginning, split dataset into two parts [0.8, 0.2] training and testing set
    :return:
    """
    df = store.read_dataset("data.csv", file_dir="hw2")
    data_sets = store.split_dataset(df, [0.8, 0.2], shuffel=True)
    store.dump_dataset(data_sets[0], f"train_out_{len(data_sets[0])}.csv", file_dir="hw2/split")
    store.dump_dataset(data_sets[1], f"train_out_{len(data_sets[1])}.csv", file_dir="hw2/split")


def subtask_i_a():
    """
    Task I)
    Perform a grid search in order to get the best hyper parameters

    The hyperparameters to tune are:
    depth                       ->  combined        combined in layer_spec
    lenght of each layer        ->  combined        (a list of the layer with the depth of each component)
                                =>  all combinations of: [30, 25, 20, 15, 10]
    learning rate               ->  [0.001, 0.0001, 0.00001]
    epochs/nr iterations        ->  [40, 50, 60]
    batch size                  ->  [32, 64, 128, 256]
    for the ranges of the grid search i just did a few experimentation's for sake of computational time ;)

    all experiments where stored in a tensorboard (see: https://pypi.org/project/tensorboard/) so i had a good overview
    run:
        tensorboard --logs store/hw2/logs/hparam_tuning
        # -> in order to obtain the board (screenshots are also provided)
    run:
        rm -rf store/hw2/logs/hparam_tuning && {{rerun programm with task_i_a enables}}
        # -> in order to redo this grid search
    """
    global LAYER_SPEC
    global LEARN_RATE
    global NR_ITERATIONS
    global HP_BATCH_SIZE

    _layer_spec = []
    for depth in range(2, 3):
        _layer_spec = _layer_spec + (list(set(itertools.combinations([30, 25, 20, 15, 10], depth))))

    LAYER_SPEC = hp.HParam('layer_spec', hp.Discrete(list(map(str, _layer_spec))))
    LEARN_RATE = hp.HParam('learn_rate', hp.Discrete([0.001, 0.0001, 0.00001]))
    NR_ITERATIONS = hp.HParam('nr_iterations', hp.Discrete([40, 50, 60]))
    HP_BATCH_SIZE = hp.HParam('batch_size', hp.Discrete([32, 64, 128, 256]))

    with tf.summary.create_file_writer(f'{store.base_dir}/hw2/logs/hparam_tuning').as_default():
        hp.hparams_config(
            hparams=[LAYER_SPEC, LEARN_RATE, NR_ITERATIONS, HP_BATCH_SIZE],
            metrics=[hp.Metric('accuracy', display_name='Accuracy')],
        )
    res = []
    job_nr = 0
    df_train = store.read_dataset(f"train_out_1120.csv", file_dir="hw2/split")
    df_test = store.read_dataset(f"train_out_280.csv", file_dir="hw2/split")
    for layer_spec in LAYER_SPEC.domain.values:
        for learn_rate in LEARN_RATE.domain.values:
            for nr_iteration in NR_ITERATIONS.domain.values:
                for batch_size in HP_BATCH_SIZE.domain.values:
                    hparams = {
                        'layer_spec': layer_spec,
                        'learn_rate': learn_rate,
                        'nr_iterations': nr_iteration,
                        'batch_size': batch_size,
                    }
                    res.append(run_nn(job_nr, hparams, df_train, df_test))
                    job_nr += 1
    max_itm = max(res, key=itemgetter(0))
    print(f"Best Parameters according grid search: {max_itm}")
    store.check_create_folder(f'{store.base_dir}/hw2/logs/hparam_tuning/')
    return max_itm


def subtask_i_b():
    """
    Task I) (b)
    In here the best parameters obtained by the grid search above will be trained again
    then the:
        a) train accuracy
        b) test accuracy
        c) train loss
        d) test loss
    will be plotted
    Best Parameters where
    [30, 25]    # layer spec
    0.001      # learning rate
    50          # epochs
    64          # batch size

    => Accuracy of 97% in my run
    """
    df_train = store.read_dataset(f"train_out_1120.csv", file_dir="hw2/split")
    df_test = store.read_dataset(f"train_out_280.csv", file_dir="hw2/split")
    dnn_model = DNNModel(
        [30, 25],  # layer spec
        0.0001,  # learning rate
        50,  # epochs
        96  # batch size
    ) \
        .set_train_test_data(df_train, df_test) \
        .fit(verbose=1)
    loss, acc, _, _ = dnn_model.evaluate(verbose=1)
    helper.plot_loss_acc(dnn_model.get_history(), save_name="accuracy")
    print()
    print("############################################")
    print(f"best model with Acc: {acc:.2f} and Loss: {loss:.2f}")
    print("############################################")
    return dnn_model


def subtask_ii(dnn_model: DNNModel):
    """
    Task II)
    Compute and plot the confusion matrices from the trained model
    """

    df_train = store.read_dataset(f"train_out_1120.csv", file_dir="hw2/split")
    df_test = store.read_dataset(f"train_out_280.csv", file_dir="hw2/split")
    train_cm = dnn_model.get_confusion_matrix(df_train)
    train_cm = train_cm.numpy()
    helper.plot_confusion_matrix(train_cm, title="train confusion matrix", save_name="train_confusion_matrix")

    test_cm = dnn_model.get_confusion_matrix(df_test)
    test_cm = test_cm.numpy()
    helper.plot_confusion_matrix(test_cm, title="test confusion matrix", save_name="test_confusion_matrix")
    return train_cm, test_cm


def subtask_iii(train_cm, test_cm):
    """
    Task III)
    Computes:
        -> precision
        -> recall
        -> F1-score
    for the previous confusion matrices
    (in the report they will be computed for the reports confusion matrices)
    """
    print()
    train_c0, train_c1 = helper.compute_params(train_cm)
    print("Training (DNN), C0:")
    print(f"\tPrecision: {train_c0[0]:.2f}")
    print(f"\tRecall: {train_c0[1]:.2f}")
    print(f"\tF1-Score: {train_c0[2]:.2f}")
    print("Training (DNN), C1:")
    print(f"\tPrecision: {train_c1[0]:.2f}")
    print(f"\tRecall: {train_c1[1]:.2f}")
    print(f"\tF1-Score: {train_c1[2]:.2f}")
    print()
    test_c0, test_c1 = helper.compute_params(test_cm)
    print("Testing (DNN), C0:")
    print(f"\tPrecision: {test_c0[0]:.2f}")
    print(f"\tRecall: {test_c0[1]:.2f}")
    print(f"\tF1-Score: {test_c0[2]:.2f}")
    print("Testing (DNN), C1:")
    print(f"\tPrecision: {test_c1[0]:.2f}")
    print(f"\tRecall: {test_c1[1]:.2f}")
    print(f"\tF1-Score: {test_c1[2]:.2f}")


def subtask_v():
    """
    Subtask IV)
    Do a classification task with random forest and decision tree and calculate:
    the calc_tree mtd does the classification and printing
    1) Accuracy
    2) Precision
    3) Recall
    4) F1-Score
    """

    def calc_tree(clf, clf_name, df_train, df_test):
        x, y = helper.split_dataset(df_train, do_one_hot_encoding=False)
        clf.fit(x, y.values.ravel())
        x_test, y_test = helper.split_dataset(df_test, do_one_hot_encoding=False)
        y_pred = clf.predict(x_test)
        accuracy = accuracy_score(y_test, y_pred)
        confusion_m = confusion_matrix(y_pred, y_test)
        test_c0, test_c1 = helper.compute_params(confusion_m)
        print()
        print(f"{clf_name}, accuracy: {accuracy:.2f}")
        print(f"{clf_name}, C0:")
        print(f"\tPrecision: {test_c0[0]:.2f}")
        print(f"\tRecall: {test_c0[1]:.2f}")
        print(f"\tF1-Score: {test_c0[2]:.2f}")
        print(f"{clf_name}, C1:")
        print(f"\tPrecision: {test_c1[0]:.2f}")
        print(f"\tRecall: {test_c1[1]:.2f}")
        print(f"\tF1-Score: {test_c1[2]:.2f}")

    simplefilter(action='ignore', category=FutureWarning)
    df_train = store.read_dataset(f"train_out_1120.csv", file_dir="hw2/split")
    df_test = store.read_dataset(f"train_out_280.csv", file_dir="hw2/split")
    decision_tree = DecisionTreeClassifier(random_state=0)
    clf_name = "DecisionTree"
    calc_tree(decision_tree, clf_name, df_train, df_test)

    random_forest = RandomForestClassifier(random_state=0)
    clf_name = "RandomForest"
    calc_tree(random_forest, clf_name, df_train, df_test)


def subtask_vi(model):
    """
    Task V)

    Plot ROC and the precision-recall curve
    :param model:
    :return:
    """
    df_test = store.read_dataset(f"train_out_280.csv", file_dir="hw2/split")
    d_x, d_y = helper.get_prediction_df(df_test, model.batch_size)
    prediction = model.predict(d_x, scores_data=d_y.numpy().flatten())
    fp, tp, _ = metrics.roc_curve(d_y, prediction[:, 1])
    mean_auc = auc(fp, tp)
    helper.plot_true_false_positives(fp, tp, mean_auc, save_name="ROC")
    precision, recall, _ = precision_recall_curve(d_y, prediction[:, 1])
    average_precision = average_precision_score(d_y, prediction[:, 1])
    helper.plot_precision_recall(recall, precision, average_precision, save_name="precession-recall")


def run_nn(j_nr, hparams, df_train, df_test):
    print(f"At Job{j_nr}")
    run_dir = f'{store.base_dir}/hw2/logs/hparam_tuning/run-{j_nr}'
    dnn_model = DNNModel(
        list(map(int, ast.literal_eval(hparams['layer_spec']))),
        hparams['learn_rate'],
        hparams['nr_iterations'],
        hparams['batch_size']
    ) \
        .set_train_test_data(df_train, df_test) \
        .add_callback(tf.keras.callbacks.TensorBoard(run_dir)) \
        .add_callback(
        hp.KerasCallback(run_dir, {LAYER_SPEC: hparams['layer_spec'],
                                   LEARN_RATE: hparams['learn_rate'],
                                   NR_ITERATIONS: hparams['nr_iterations'],
                                   HP_BATCH_SIZE: hparams['batch_size']
                                   }, )) \
        .fit(verbose=0)
    print("Evaluating....")
    _, acc, _, _ = dnn_model.evaluate(verbose=0)
    print(f"Job{j_nr}\n Acc: {acc} => hparams: {hparams}")
    with tf.summary.create_file_writer(run_dir).as_default():
        tf.summary.scalar('accuracy', acc, step=1)
    return acc, hparams

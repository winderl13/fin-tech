from hw2 import helper
import tensorflow as tf
from tensorflow import keras
import pandas as pd
import numpy as np


class DNNModel:
    def __init__(self, layer_spec, learning_rate, nr_iterations, batch_size):
        self.layer_spec = layer_spec
        self.learning_rate = learning_rate
        self.nr_iterations = nr_iterations
        self.batch_size = batch_size
        self.ds_train = None
        self.ds_test = None
        self.input_size = None
        self.model: keras.Sequential = None
        self.train_hist = None
        self.callbacks = []

    def set_train_test_data(self, df_train, df_test):
        self.ds_train, _ = helper.prepare_dataset(df_train, batch_size=self.batch_size)
        self.ds_test, self.input_size = helper.prepare_dataset(df_test, batch_size=self.batch_size)
        return self

    def add_callback(self, callback):
        self.callbacks.append(callback)
        return self

    def build(self):
        model = keras.Sequential()
        model.add(keras.layers.Dense(units=self.layer_spec[0],
                                     input_shape=(self.input_size,)))
        for layer_size in self.layer_spec[1:]:
            model.add(keras.layers.Dense(units=layer_size, activation='relu'))
        model.add(keras.layers.Dense(units=2, activation='softmax'))

        model.compile(
            loss=keras.losses.CategoricalCrossentropy(from_logits=False),
            optimizer=tf.keras.optimizers.Adam(learning_rate=self.learning_rate, amsgrad=False),
            metrics=['accuracy', tf.keras.metrics.Recall(), tf.keras.metrics.Precision()],
        )
        self.model = model
        return self

    def fit(self, verbose=1):
        assert self.ds_train is not None
        assert self.ds_test is not None
        assert self.input_size is not None
        self.build()
        assert self.model is not None
        self.train_hist = self.model.fit(self.ds_train,
                                         verbose=verbose,
                                         epochs=self.nr_iterations,
                                         validation_data=self.ds_test,
                                         callbacks=self.callbacks,
                                         use_multiprocessing=True)
        if verbose > 0:
            self.model.summary()
        return self

    def evaluate(self, verbose=1):
        return self.model.evaluate(self.ds_test, verbose=verbose)

    def predict(self, d_x, scores_data=None):
        y_ = self.model.predict(d_x)
        if scores_data is None:
            y_ = list(map(np.argmax, y_))
        return y_

    def get_confusion_matrix(self, df: pd.DataFrame):
        assert self.model is not None
        assert self.train_hist is not None
        d_x, d_y = helper.get_prediction_df(df, self.batch_size)
        y_pred = self.predict(d_x)
        confusion_m = tf.math.confusion_matrix(y_pred, d_y, num_classes=2)
        return confusion_m

    def get_history(self):
        return self.train_hist.history

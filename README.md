## Finance Technology Homeworks

This project encapsulates all FinTech Homeworks

##Installation

to install the reqired dependencys run 
```
pipenv install
```
The current dependencys are:

```
- numpy
- pandas

```

## Launch
Launch the project via Pipenv to make shure dependencys are set
```
pipenv run python main.py
```
launch the individual homework vial cli parameters:
```
pipenv run python main.py -hw <hw_nr> [OPTIONS]
```
Options are defined for different homeworks as the following:
* hw1:
    - None